/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Input.hpp"
#include <sstream>
#include <regex>

void Input::newFilename(const std::string& name) noexcept
{
	_filename = name;

	_filenameWasSet = true;
}

void Input::newNumber(const std::string& number)
{
	if(std::regex_match(number, std::regex("^[1-9][0-9]*$")))
		_number = number.c_str();
	else
		throw std::invalid_argument("Number has wrong format!");

	_numberWasSet = true;
}

void Input::newWidth(const std::string& width) 
{
	if(std::regex_match(width, std::regex("^[1-9][0-9]*$")))
		_width = width;
	else
		throw std::invalid_argument("Width has wrong format!");
}

void Input::newHeight(const std::string& height)
{
	if(std::regex_match(height, std::regex("^[1-9][0-9]*$")))
		_height = height;
	else
		throw std::invalid_argument("Height has wrong fromat!");
}

std::string Input::filename() const noexcept
{
	return _filename;
}

const mpz_class& Input::number() const noexcept
{
	return _number;
}

std::size_t Input::width() const noexcept
{
	return stosize_t(_width);
}

std::size_t Input::height() const noexcept
{
	return stosize_t(_height);
}

std::size_t Input::stosize_t(const std::string& s) noexcept
{
	std::stringstream ss(s);
	std::size_t toReturn = 0U;

	//guranteed to succeed
	ss >> toReturn;

	return toReturn;
}

bool Input::wasFilenameSet() const noexcept
{
	return _filenameWasSet;
}

bool Input::wasNumberSet() const noexcept
{
	return _numberWasSet;
}
