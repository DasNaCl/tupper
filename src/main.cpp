/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// The output of this program relies on the inequation of Figure 13 from the
// paper written by Jeff Tupper aviable at the following address:
// http://www.dgp.toronto.edu/people/mooncake/papers/SIGGRAPH2001_Tupper.pdf


#include "../include/Application.hpp"
#include <vector>
#include <string>

int main(int argc, char** argv)
{
	std::vector<std::string> arguments;

	for(int i = 1; i < argc; ++i)
		arguments.push_back(argv[i]);

	Application app;
	app.run(arguments);
}
