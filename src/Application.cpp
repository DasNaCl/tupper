/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Application.hpp"
#include "../include/Plotter.hpp"
#include "../include/Reader.hpp"
#include "../include/Input.hpp"
#include <iostream>
#include <stdexcept>

void Application::run(const std::vector<std::string>& arguments) noexcept
{
	Input input;
	try
	{
		readArguments(arguments, input);
	}
	catch(std::exception& e)
	{
		std::cout << e.what() << std::endl;
		return;
	}
	
	if(input.wasNumberSet())
		Plotter(input.number(), input.filename(),
			{input.width(), input.height()});
	else
	{
		if(input.wasFilenameSet())
		{
			try
			{
				Reader({input.width(), input.height()},
					input.filename());
			}
			catch(std::exception& e)
			{
				std::cout << e.what() << std::endl;
			}
		}
		else
			std::cout << "Please provide a filename." << std::endl;
	}
}

void Application::readArguments(const std::vector<std::string>& arguments,
				Input& input)
{
	for(auto it = std::begin(arguments); it != std::end(arguments); ++it)
	{
		if(*it == "-fn"
		|| *it == "--file-name")
		{
			++it;

			if(it != std::end(arguments))
				input.newFilename(*it);
			else
				throw std::invalid_argument("Empty name!");
		}
		else if(*it == "-ik"
		     || *it == "--input-k")
		{
			++it;

			if(it != std::end(arguments))
				input.newNumber(*it);
			else
				throw std::invalid_argument("Empty number!");
		}
		else if(*it == "-w"
		     || *it == "--width")
		{
			if(it != std::end(arguments))
				input.newWidth(*it);
			else
				throw std::invalid_argument("Empty width!");
		}
		else if(*it == "-he"
		     || *it == "--height")
		{
			if(it != std::end(arguments))
				input.newHeight(*it);
			else
				throw std::invalid_argument("Empty height!");
		}
		else
		{
			if(*it != "-h" && *it != "--help" && *it != "-?")
				throw std::invalid_argument("\""+*it+"\" is unknown.");
			printHelp();
		}
	}
}

void Application::printHelp() const noexcept
{
	std::cout << "Tupper [arguments]\tPossible arguments:\n";
	std::cout << "\t-h -? --help\tPrint this help here.\n";
	std::cout << "\t-fn --file-name\tSet a file name.\n";
	std::cout << "\t-ik --input-k\tSet specific y for plotting.\n";
	std::cout << "\t-w --width\tSet the width of the plot.\n";
	std::cout << "\t-he --height\tSet the height of the plot.\n";
}
