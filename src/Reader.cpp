/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Reader.hpp"
#include <SFML/Graphics/Image.hpp>
#include <iostream>
#include <gmpxx.h>
#include <sstream>

Reader::Reader(sf::Vector2<std::size_t> bounds,
		const std::string& filename)
		
{
	sf::Image img;
	if(!img.loadFromFile(filename))
		throw filename+" not accessible.";

	std::stringstream kSS;
	for(std::size_t x = 0ULL; x < bounds.x; ++x)
	{
		for(std::size_t y = 0ULL; y < bounds.y; ++y)
		{
			if(img.getPixel(x, bounds.y-1ULL-y) == sf::Color::Black)
				kSS << "1";
			else
				kSS << "0";
		}
	}

	mpz_class k(kSS.str(), 2);
	k = k * static_cast<unsigned long>(bounds.y);

	std::cout << k << std::endl;
}
