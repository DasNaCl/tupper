/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Plotter.hpp"
#include <SFML/Graphics/Image.hpp>
#include <sstream>

Plotter::Plotter(const mpz_class& k,
		 std::string filename,
		 sf::Vector2<std::size_t> bounds) noexcept
	: _bounds(bounds)
{
	sf::Image plot;
	plot.create(_bounds.x, _bounds.y, sf::Color::White);

	for(std::size_t x = 0ULL; x < bounds.x; ++x)
	{
		for(std::size_t y = 0ULL; y < bounds.y; ++y)
		{
			mpz_class fy = k + y;
			if(tupper(x, fy))
				plot.setPixel(bounds.x - 1ULL - x, y,
						sf::Color::Black);
		}
	}

	plot.saveToFile(filename);
}

bool Plotter::tupper(const mpz_class& x, const mpz_class& y) noexcept
{
	std::stringstream heightSS;
	heightSS << _bounds.y;

	mpz_class ex = (x * _bounds.y + (y % mpz_class(heightSS.str())));
	
	mpz_class fx;
	mpz_ui_pow_ui(fx.get_mpz_t(), 2U, ex.get_ui());

	return 0.5 < (((y / _bounds.y) / fx) % mpz_class("2"));
}
