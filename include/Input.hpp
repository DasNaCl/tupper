/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include <string>
#include <gmpxx.h>

class Input
{
private:
	static constexpr const char* myself() noexcept;
public:
	Input() = default;

	void newFilename(const std::string& name) noexcept;
	void newNumber(const std::string& number);
	void newWidth(const std::string& width);
	void newHeight(const std::string& height);

	std::string filename() const noexcept;
	const mpz_class& number() const noexcept;
	std::size_t width() const noexcept;
	std::size_t height() const noexcept;

	bool wasNumberSet() const noexcept;
	bool wasFilenameSet() const noexcept;
private:
	static std::size_t stosize_t(const std::string& s) noexcept;
private:
	std::string _filename {"some_plot.png"};
	mpz_class _number     {myself()};
	std::string _width    {"107"};
	std::string _height   {"17"};

	bool _filenameWasSet  {false};
	bool _numberWasSet    {false};
};

//due to its constexpr-ness it has to be defined here in the header
constexpr const char* Input::myself() noexcept
{
	//returning specific constant which will plot the general used equation
	return  "4858450636189713423582095962494202044581400587983244549483093"
		"0850619347047088099284506447698655243648499972470249151191104"
		"1160573917740785691975432657185544205721044573588368182982375"
		"4139634338225199452191651284348332905131193199953502413758765"
		"2392648746133949068701305622958132194811136853395355652908500"
		"2387509285689269455597428154638651073004910672305893358605254"
		"4096664351265349363643957125565695936815184334857605266940161"
		"2512669514215505395545191537854575257565907405401579290017659"
		"67965480064427829131488548259914721248506352686630476300";
}
